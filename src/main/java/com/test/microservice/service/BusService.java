package com.test.microservice.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.test.microservice.Model.Bus;
import com.test.microservice.Model.Seat;


@Service
public class BusService {

	List<Bus> busList= Arrays.asList(
			new Bus(1,"SRS travels"),
			new Bus(2,"TAT travels")
			);
	
	@Autowired
	private RestTemplate restTemplate;
	
	
	public Bus getBusList(Integer busId, Integer customerId){
		
		
		Seat[] seat =  restTemplate.getForObject("http://localhost:3001/seat/"+busId+"/"+customerId, Seat[].class);
		Bus busToBeReturned = new Bus();
		for(Bus bus: busList) {
			if(bus.getBusId().equals(busId)) {
				busToBeReturned.setBusId(busId);
				busToBeReturned.setBusName(bus.getBusName());
			}
		}
		busToBeReturned.setSeatlist(Arrays.asList(seat));
		
		
		
		
		return busToBeReturned;
		
		
	}
}
