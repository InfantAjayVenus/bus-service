package com.test.microservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.microservice.Model.Bus;
import com.test.microservice.service.BusService;

@RestController
@RequestMapping(value = "/bus")
public class BusController {
	
	@Autowired
	private BusService busservice;

	@GetMapping(value="/getList/{busId}/{customerId}")
	public Bus getBusList(@PathVariable(name="busId", required=true) Integer busId,@PathVariable(name="customerId", required=true) Integer customerId) {
		return busservice.getBusList(busId,customerId);
		
	}
}
